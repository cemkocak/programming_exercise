Fill in the blanks in each of the following statements:

a-) Devices from which users access timesharing computer systems are usually called "terminal".

b-) A computer program that converts assembly language programs to machine language programs is called "assember".

c-) The logical unit of the computer that receives information from outside the computer for use by the computer is called "input unit".

d-) The process of instructing the computer to solve specific problems is called "runtime".

e-) What type of computer language uses English-like abbreviations for machine language instructions? "Assembly"

f-) Which logical unit of the computer sends information that has already been processed by the computer to various devices, so that the information 
may be used outside the computer? "Output unit"

g-) The general name for a program that converts programs written in a certain computer language into machine language is "compiler".

h-) Which logical unit of the computer retains information? "Memory unit"

i-) Which logical unit of the computer performs calculations? "ALU"

j-) Which logical unit of the computer makes logical decisions? "ALU"

k-) The commonly used abbreviation for the computer's control unit is "CPU".

l-) The level of computer language most convenient to the programmer for writing programs quickly and easily is "high-level".

m-) The only language that a computer can directly understand is called that computer's "natural language".

n-) Which logical unit of the computer coordinates the activities of all the other logical units? "CPU"